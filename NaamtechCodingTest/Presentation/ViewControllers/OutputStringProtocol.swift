//
//  OutputStringProtocol.swift
//  NaamtechCodingTest
//
//  Created by Robert Bernardini on 8/31/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

import Foundation


// MARK: - OutputStringProtocol

// Protocol to be implemented to generate and output the solution for an input string
protocol OutputStringProtocol {
    func generateOutputForString(_ inputString: String) -> String
}
