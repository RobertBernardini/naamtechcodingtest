//
//  TestOneViewController.swift
//  NaamtechCodingTest
//
//  Created by Robert Bernardini on 8/30/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

import UIKit


// MARK: - TestOneViewController
class TestOneViewController: BaseViewController {

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}


// MARK: - OutputStringProtocol
extension TestOneViewController: OutputStringProtocol {
    
    func generateOutputForString(_ inputString: String) -> String {
        return inputString.printPositionOfCharacter("a")
    }
}


