//
//  TestTwoViewController.swift
//  NaamtechCodingTest
//
//  Created by Robert Bernardini on 8/30/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

import UIKit


// MARK: - TestTwoViewController
class TestTwoViewController: BaseViewController {

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}


// MARK: - OutputStringProtocol
extension TestTwoViewController: OutputStringProtocol {
    
    func generateOutputForString(_ inputString: String) -> String {
        return inputString.generateCharacterReport()
    }
}
