//
//  BaseViewController.swift
//  NaamtechCodingTest
//
//  Created by Robert Bernardini on 8/30/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

import UIKit

// MARK: - BaseViewController
class BaseViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var outputLabel: UILabel!
    @IBOutlet weak var titleLabelTopConstraint: NSLayoutConstraint!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabelTopConstraint.constant = UIDevice.current.userInterfaceIdiom == .phone ? 30 : 100
    }
}


// MARK: - IBActions
extension BaseViewController {
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        inputTextField.resignFirstResponder()
        processInputString(inputTextField.text)
    }
}


// MARK: - Public Methods
extension BaseViewController {
    
    func processInputString(_ userString: String?) {
        // If child class implements OutputStringProtocol process the input string and output its result to the output label.
        if let inputString = userString, let outputStringProtocolViewController = self as? OutputStringProtocol {
            let outputString = outputStringProtocolViewController.generateOutputForString(inputString)
            outputLabel.text = outputString
            return
        }
    }
}


// MARK: - UITextFieldDelegate
extension BaseViewController: UITextFieldDelegate {
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        processInputString(textField.text)
        return true
    }
}
