//
//  TestThreeViewController.swift
//  NaamtechCodingTest
//
//  Created by Robert Bernardini on 8/30/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//

import UIKit


// MARK: - TestThreeViewController
class TestThreeViewController: BaseViewController {

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}


// MARK: - OutputStringProtocol
extension TestThreeViewController: OutputStringProtocol {
    
    func generateOutputForString(_ inputString: String) -> String {
        let stringCalculator = StringCalculator(inputString: inputString)
        return stringCalculator.solveInputEquation()
    }
}
