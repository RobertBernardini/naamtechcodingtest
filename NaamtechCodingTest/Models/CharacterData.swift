//
//  CharacterData.swift
//  NaamtechCodingTest
//
//  Created by Robert Bernardini on 9/31/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//
// Class used to store the character and count for each character in Coding Test 2


import UIKit

// MARK: - CharacterData
class CharacterData: NSObject {
    
    // MARK: - Properties
    var character: Character
    var count: Int
    
    // MARK: - Lifecycle
    init(character: Character, count: Int) {
        self.character = character
        self.count = count
    }
}
