//
//  StringCalculator.swift
//  NaamtechCodingTest
//
//  Created by Robert Bernardini on 8/31/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//
// Calculator struct to parse, validate and solve the input mathematical string equation

import UIKit


// MARK: - Enum
// Enum to detect operation type and to solve the desired operation
enum Operation: Character {
    
    case addition = "+"
    case subtraction = "-"
    case multiplication = "x"
    case division = "/"
    case modulus = "%"
    
    func operate(firstOperand: Float, secondOperand: Float) -> Float? {
        switch self {
        case .multiplication:
            return firstOperand*secondOperand
        case .division:
            return secondOperand != 0 ? firstOperand/secondOperand : nil
        case .modulus:
            return secondOperand != 0 ? Float(Int(firstOperand)%Int(secondOperand)) : nil
        case .addition:
            return firstOperand+secondOperand
        case .subtraction:
            return firstOperand-secondOperand
        }
    }
}


// MARK: - StringCalculator
struct StringCalculator {
    
    // MARK: - Properties
    let inputString: String

    // MARK: - Lifecycle
    init(inputString: String) {
        self.inputString = inputString
    }
}


// MARK: - Public Methods
extension StringCalculator {
    
    // Function to solve the input equation and to return an output string with the result.
    func solveInputEquation() -> String {
        // Parse string to remove whitespaces
        let equationString = inputString.removeWhiteSpaces()
        
        // Test if string is empty and if string can be compartmentalized into an array of operators and operands otherwise
        // return Syntax Error.
        guard equationString.count != 0, let equationComponents = equationString.compartmentalizeEquationString() else {
            return "Error: Syntax Error"
        }
        
        // Solve equation. If nil returned it is due to being divided by zero.
        guard let solution = solveEquation(equationComponents) else {
            return "Error: Division by zero"
        }
        return String(solution)
    }
}

// MARK: - Private Methods
extension StringCalculator {
    // Private function to solve the compartmentalized equation
    private func solveEquation(_ equationComponents: [Any]) -> Int? {
        var components = equationComponents
        var index = 1
        var hasLooped = 0
        var result: Float? = components.first as? Float
        var tempComponents = [Any]()

        // Loop through the equation two times: the first time to solve the multiplication, division and modulus operations,
        // and the second time to solve the addition and subtraction operations
        while index < components.count && hasLooped < 2 {
            let tempFirstOperand = components[index-1] as! Float
            let firstOperand = result == nil ? tempFirstOperand : result!   // Use result from previous operation otherwise original first operand as first operand
            let secondOperand = components[index+1] as! Float
            let equationOperator = components[index] as! Character
            let operation = Operation(rawValue: equationOperator)!

            // First loop
            if hasLooped == 0 {
                switch operation {
                // If multiplication, division or modulus operation, operate and store in result variable
                case Operation.multiplication, Operation.division, Operation.modulus:
                    result = operation.operate(firstOperand: firstOperand, secondOperand: secondOperand)
                    if result == nil {
                        return nil
                    }
                // If addition or subtraction operation, store in tempComponents array for second loop
                default:
                    tempComponents.append(firstOperand)
                    tempComponents.append(equationOperator)
                    result = nil
                }
            } else {
                // Second loop, solve addition and subtraction operations and store in result
                result = operation.operate(firstOperand: firstOperand, secondOperand: secondOperand)
            }
            index = index + 2

            // Having iterated through equation:
            // If first loop, append secondOperand to end of tempComponents array and assign that array to components
            // array to be iterated in the second loop, otherwise iterate index and loop counters and exit while loop.
            if index == components.count {
                let operandToAppend = result == nil ? secondOperand : result!   // Use result from last operation otherwise original second operand as final operand to append
                tempComponents.append(operandToAppend)
                components = tempComponents
                hasLooped = hasLooped + 1
                index = 1
                result = nil
                tempComponents.removeAll()
            }
        }
        let finalResult = components.first as! Float
        return Int(finalResult)
    }
}
