
//
//  String+Parser.swift
//  NaamtechCodingTest
//
//  Created by Robert Bernardini on 9/31/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//
// Extension of String to perform Coding Challenge tests. I found it more logical to create an extension to perform these tests.


import Foundation

// MARK: - Public methods
extension String {
    
    // Function to print the position of the given character.
    func printPositionOfCharacter(_ character: Character) -> String {
        if let position = findPositionOfCharacter(character, startingAtIndex: 0) {
            return "Position: \(position)"
        }
        return "Position: N/A"
    }
    
    // Function to generate the Character Report for the input string.
    func generateCharacterReport() -> String {
        var characterReport = "=========\n==Report=="
        var characterCount = 0
        var totalCharacterCount = 0
        var characterDataArray = [CharacterData]()
        
        for character in self {
            // If character in string is the same as the previous character, increment the count for that character
            if let characterData = characterDataArray.last, character == characterData.character {
                characterCount = characterCount + 1
                characterData.count = characterCount
            } else {
                // Create and append a new CharacterData model for the new character in the string
                characterCount = 1
                let characterData = CharacterData(character: character, count: characterCount)
                characterDataArray.append(characterData)
            }
            totalCharacterCount = totalCharacterCount + 1
        }
        
        // Iterate through CharacterData models and generate report for each character
        for characterData in characterDataArray {
            let characterString = (characterData.character == " ") ? "␣" : "\(characterData.character)"
            characterReport.append("\n\(characterString):\t\(characterData.count)")
        }
        
        // Add total character count to report
        characterReport.append("\nTotal Characters:\t\(totalCharacterCount)")
        return characterReport
    }
    
    // Function to remove whitespaces (used for Coding Test 3).
    func removeWhiteSpaces() -> String {
        var newString = ""
        
        for character in self {
            if character != " " {
                newString.append(character)
            }
        }
        return newString
    }
    
    // Function to separate the input string equation into an array of operands and operators.
    // If the equation is not valid the function will return nil otherwise it will return the array.
    func compartmentalizeEquationString() -> [Any]? {
        var equationComponents = [Any]()
        var numberString = ""
        var previousCharacterIsMathSymbol = false
        
        for characterIndex in 0..<self.count {
            let character = self[index(startIndex, offsetBy: characterIndex)]
            let isNumericCharacter = Int(String(character)) != nil
            
            switch isNumericCharacter {
            // Case numeric character append to array as a float.
            case true:
                numberString.append(character)
                
                if characterIndex + 1 == self.count, let number = Float(numberString) {
                    equationComponents.append(number)
                }
                previousCharacterIsMathSymbol = false
                continue
                
            // Case character
            default:
                // For previously stored number, add it to the array.
                if numberString.count > 0, let number = Float(numberString) {
                    equationComponents.append(number)
                    numberString = ""
                }

                switch (character, self.count, characterIndex, previousCharacterIsMathSymbol, numberString.count) {
                // Case first character in the string is a negative symbol or the previous character was an operator symbol,
                // there is more than one character in the string, and it is not the last character in the string.
                case ("-", 2...Int.max, 0, false, 0), ("-", 2...Int.max, 0...self.count-2, true, 0):
                    numberString.append(character)
                    previousCharacterIsMathSymbol = true
                    continue
                // Case operator symbol and the previous character was not an operator symbol, there is more than one
                // chracter in the string and not the last character.
                case (let operateCharacter, 2...Int.max, 0...self.count-2, false, 0) where operateCharacter == "+" || operateCharacter == "-" || operateCharacter == "x" || operateCharacter == "/" || operateCharacter == "%":
                    equationComponents.append(character)
                    previousCharacterIsMathSymbol = true
                // All other cases.
                default:
                    return nil
                }
            }
        }
        return equationComponents
    }
}


// MARK: - Private methods
extension String {
    
    // Recursive function to find the position of the given character.
    private func findPositionOfCharacter(_ character: Character, startingAtIndex characterIndex: Int) -> Int? {
        if count == 0 || characterIndex == count {
            return nil
        }
        
        let testCharacter = self[index(startIndex, offsetBy: characterIndex)]
        if character == testCharacter {
            return characterIndex + 1
        }
        
        return findPositionOfCharacter(character, startingAtIndex: characterIndex + 1)
    }
}
