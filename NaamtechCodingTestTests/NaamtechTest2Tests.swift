//
//  NaamtechTest2Tests.swift
//  NaamtechCodingTestTests
//
//  Created by Robert Bernardini on 9/4/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//
// Tests for Coding Test 2


import XCTest
@testable import NaamtechCodingTest


class NaamtechTest2Tests: XCTestCase {

    func testStringGeneratedReportForSimpleString() {
        
        // Given
        let testString = "a_++efg"
        
        // When
        let characterPosition = testString.generateCharacterReport()
        
        // Then
        let result = characterPosition == "=========\n==Report==\na:\t1\n_:\t1\n+:\t2\ne:\t1\nf:\t1\ng:\t1\nTotal Characters:\t7"
        XCTAssert(result)
    }
    
    func testStringGeneratedReportForStringWithSeparatedRepeatedCharacters() {
        
        // Given
        let testString = "a_++efg++"
        
        // When
        let characterPosition = testString.generateCharacterReport()
        
        // Then
        let result = characterPosition == "=========\n==Report==\na:\t1\n_:\t1\n+:\t2\ne:\t1\nf:\t1\ng:\t1\n+:\t2\nTotal Characters:\t9"
        XCTAssert(result)
    }
    
    func testStringGeneratedReportForStringWithWhiteSpaces() {
        
        // Given
        let testString = "a_++   efg2"
        
        // When
        let characterPosition = testString.generateCharacterReport()
        
        // Then
        let result = characterPosition == "=========\n==Report==\na:\t1\n_:\t1\n+:\t2\n␣:\t3\ne:\t1\nf:\t1\ng:\t1\n2:\t1\nTotal Characters:\t11"
        XCTAssert(result)
    }
    
    func testStringGeneratedReportForLargeStringWithSpaces() {
        
        // Given
        let testString = "a_++   ;?aa  efg2"
        
        // When
        let characterPosition = testString.generateCharacterReport()
        
        // Then
        let result = characterPosition == "=========\n==Report==\na:\t1\n_:\t1\n+:\t2\n␣:\t3\n;:\t1\n?:\t1\na:\t2\n␣:\t2\ne:\t1\nf:\t1\ng:\t1\n2:\t1\nTotal Characters:\t17"
        XCTAssert(result)
    }
    
    func testStringGeneratedReportForEmptyString() {
        
        // Given
        let testString = ""
        
        // When
        let characterPosition = testString.generateCharacterReport()
        
        // Then
        let result = characterPosition == "=========\n==Report==\nTotal Characters:\t0"
        XCTAssert(result)
    }
    
    func testStringGeneratedReportForWhitespaceString() {
        
        // Given
        let testString = "   "
        
        // When
        let characterPosition = testString.generateCharacterReport()
        
        // Then
        let result = characterPosition == "=========\n==Report==\n␣:\t3\nTotal Characters:\t3"
        XCTAssert(result)
    }
}
