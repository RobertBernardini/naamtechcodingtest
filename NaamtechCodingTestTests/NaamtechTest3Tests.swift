//
//  NaamtechTest3Tests.swift
//  NaamtechCodingTestTests
//
//  Created by Robert Bernardini on 9/4/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//
// Tests for Coding Test 3


import XCTest
@testable import NaamtechCodingTest


class NaamtechTest3Tests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testEquationStringWithPositiveOutput() {
        
        // Given
        let testString = "3 x 3 + 3 - 2"
        let stringCalculator = StringCalculator(inputString: testString)

        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "10")
    }
    
    func testEquationStringWithNegativeNumber() {
        
        // Given
        let testString = "3 x 3 + 3 - -2"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "14")
    }
    
    func testEquationStringWithSyntaxError() {
        
        // Given
        let testString = "1 ++ 2 x 2 x / 4 + 0"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithSyntaxErrorTripleNegative() {
        
        // Given
        let testString = "1 --- 2 x 2 x 5 / 4 + 0"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithSyntaxErrorDoubleNegativeAtBeginning() {
        
        // Given
        let testString = "  -  -  1 - 2 x 2 x 5/ 4 + 0"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithNegativeAtBeginning() {
        
        // Given
        let testString = "  -1 - 2 x 2 x 5/ 4 + 0"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "-6")
    }
    
    func testEquationStringWithNegativeOutput() {
        
        // Given
        let testString = "2 x 3 + 2 + 5 + 7 - 55 - 0 - 12 - 4 / 17 / 3 x 7 + 2 - 16"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "-61")
    }
    
    func testEquationStringWithDivisionByZero() {
        
        // Given
        let testString = "1 + 1 - 1 x 2 / 0"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Division by zero")
    }
    
    func testEquationStringWithMultiplicationAsLastOperation() {
        
        // Given
        let testString = "5 + 1 - 1 x 2 x 10"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "-14")
    }
    
    func testEquationStringWithMultiplicationByZeroAsLastOperation() {
        
        // Given
        let testString = "5 + 1 - 1 x 2 x 0"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "6")
    }
    
    func testEquationStringWithDivisionAsLastOperation() {
        
        // Given
        let testString = "5 + 1 - 1 x 2 / 5"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "5")
    }
    
    func testEquationStringWithModulusOperation() {
        
        // Given
        let testString = "1 % 3 - 1"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "0")
    }
    
    func testEquationStringWithModulusByZero() {
        
        // Given
        let testString = "1 % 0 - 1"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Division by zero")
    }
    
    func testEquationStringWithModulusSyntaxError() {
        
        // Given
        let testString = "1 %% 3 - 1"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithModulusByNegativeNumber() {
        
        // Given
        let testString = "1 % -3 - 1"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "0")
    }
    
    func testEquationEmptyString() {
        
        // Given
        let testString = ""
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationWhitespaceString() {
        
        // Given
        let testString = "   "
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithSymbols() {
        
        // Given
        let testString = "1 + 1 & 1 x 2 / 0"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithOneDigit() {
        
        // Given
        let testString = "5"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "5")
    }
    
    func testEquationStringWithTwoDigits() {
        
        // Given
        let testString = "15"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "15")
    }
    
    func testEquationStringWithOneNegativeSymbol() {
        
        // Given
        let testString = "-"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithTwoNegativeSymbols() {
        
        // Given
        let testString = " -  -"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithTwoNegativeSymbolsAndNumber() {
        
        // Given
        let testString = " -  -"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithNegativeSymbolAndNumber() {
        
        // Given
        let testString = " -  5"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "-5")
    }
    
    func testEquationStringWithThreeNegativeSymbosl() {
        
        // Given
        let testString = " -  -   -  "
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithOneSymbol() {
        
        // Given
        let testString = "+"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithTwoSymbols() {
        
        // Given
        let testString = "+  +"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithNonConformingSymbol() {
        
        // Given
        let testString = "  ^ "
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringWithNormalAndNonConformingSymbol() {
        
        // Given
        let testString = "+ ^ "
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringTerminatingWithSubtractionSymbol() {
        
        // Given
        let testString = "5-2-"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringTerminatingWithDoubleSubtractionSymbol() {
        
        // Given
        let testString = "5-2--"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringTerminatingWithTripleSubtractionSymbol() {
        
        // Given
        let testString = "5-2---"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringTerminatingWithAdditionSymbol() {
        
        // Given
        let testString = "5-2 +"
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringTerminatingWithDoubleAdditionSymbol() {
        
        // Given
        let testString = "5-2 +  +  "
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
    
    func testEquationStringTerminatingWithNonConformingSymbol() {
        
        // Given
        let testString = "5-  2* "
        let stringCalculator = StringCalculator(inputString: testString)
        
        // When
        let solution = stringCalculator.solveInputEquation()
        
        // Then
        XCTAssert(solution == "Error: Syntax Error")
    }
}
