//
//  NaamtechTest1Tests.swift
//  NaamtechCodingTestTests
//
//  Created by Robert Bernardini on 9/4/18.
//  Copyright © 2018 Robert Bernardini. All rights reserved.
//
// Tests for Coding Test 1


import XCTest
@testable import NaamtechCodingTest


class NaamtechTest1Tests: XCTestCase {

    func testStringContainsCharacter() {
        
        // Given
        let testString = "bcdefgabcdefg"
        
        // When
        let characterPosition = testString.printPositionOfCharacter("a")
        
        // Then
        XCTAssert(characterPosition == "Position: 7")
    }
    
    func testStringDoesNotContainCharacter() {
        
        // Given
        let testString = "bcdefgbcdefg"
        
        // When
        let characterPosition = testString.printPositionOfCharacter("a")
        
        // Then
        XCTAssert(characterPosition == "Position: N/A")
    }
    
    func testStringContainsCharacterAtFirstPosition() {
        
        // Given
        let testString = "abcdefgbcdefg"
        
        // When
        let characterPosition = testString.printPositionOfCharacter("a")
        
        // Then
        XCTAssert(characterPosition == "Position: 1")
    }
    
    func testStringContainsCharacterAtLastPosition() {
        
        // Given
        let testString = "bcdefgbcdefga"
        
        // When
        let characterPosition = testString.printPositionOfCharacter("a")
        
        // Then
        XCTAssert(characterPosition == "Position: 13")
    }
    
    func testStringContainsCharacterTwice() {
        
        // Given
        let testString = "bcdefagbcdefga"
        
        // When
        let characterPosition = testString.printPositionOfCharacter("a")
        
        // Then
        XCTAssert(characterPosition == "Position: 6")
    }
    
    func testStringContainsCharacterInStringWithSymbols() {
        
        // Given
        let testString = "b•c/.aefg•cdefg"
        
        // When
        let characterPosition = testString.printPositionOfCharacter("a")
        
        // Then
        XCTAssert(characterPosition == "Position: 6")
    }
    
    func testStringContainsCharacterInStringWithSymbolsSpaces() {
        
        // Given
        let testString = "b•c3d?  efagbcdefg"
        
        // When
        let characterPosition = testString.printPositionOfCharacter("a")
        
        // Then
        XCTAssert(characterPosition == "Position: 11")
    }
    
    func testStringDoesNotContainCharacterWithSymbolsSpaces() {
        
        // Given
        let testString = "bcde •@fgbcdefg"
        
        // When
        let characterPosition = testString.printPositionOfCharacter("a")
        
        // Then
        XCTAssert(characterPosition == "Position: N/A")
    }
    
    func testStringEmptyString() {
        
        // Given
        let testString = ""
        
        // When
        let characterPosition = testString.printPositionOfCharacter("a")
        
        // Then
        XCTAssert(characterPosition == "Position: N/A")
    }
    
    func testStringWhitespaceString() {
        
        // Given
        let testString = "   "
        
        // When
        let characterPosition = testString.printPositionOfCharacter("a")
        
        // Then
        XCTAssert(characterPosition == "Position: N/A")
    }
}
